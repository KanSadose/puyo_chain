//============================================================================
// Name        : Puyo_Max_Chain.cpp
// Author      : Kan Sadose
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : puyo tansaku in C++, Ansi-style
//============================================================================

#include <iostream>
#include "game.h"
using namespace std;

int main() {
	Game puyo(6*13);
	//Game puyo;
	puyo.print_map(0);
	puyo.do_chain();
	return 0;
}
