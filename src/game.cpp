/*
 * game.cpp
 *
 *  Created on: 2017/06/15
 *      Author: sadose
 */

#include "game.h"


/*--------------------------------------------------------------------------*/
Game::Game(){
	int list_num;
	std::random_device rnd;     // 非決定的な乱数生成器を生成
	std::mt19937 mt(rnd());     //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
	std::uniform_int_distribution<> rand100(1, 4);        // [1, 4] 範囲の一様乱数
	m.num = 12;
	//1:red 2:green 3:yellow 4:blue 0:empty
	//上下逆転で入力
	int puyo[6*13] = {
			                   /*-----------------*/
			1, 2 ,3, 4, 1, 2,  /*0  1  2  3  4  5 */
			3, 4, 1, 2, 3, 4,  /*6  7  8  9  10 11*/
			0, 0, 0, 0, 0, 0,  /*12 13 14 15 16 17*/
			0, 0, 0, 0, 0, 0,  /*18 19 20 21 22 23*/
			0, 0, 0, 0, 0, 0,  /*24 25 26 27 28 29*/
			0, 0, 0, 0, 0, 0,  /*30 31 32 33 34 35*/
			0, 0, 0, 0, 0, 0,  /*36 37 38 39 40 41*/
			0, 0, 0, 0, 0, 0,  /*42 43 44 45 46 47*/
			0, 0, 0, 0, 0, 0,  /*48 49 50 51 52 53*/
			0, 0, 0, 0, 0, 0,  /*54 55 56 57 58 59*/
			0, 0, 0, 0, 0, 0,  /*60 61 62 63 64 65*/
			0, 0, 0, 0, 0, 0,  /*66 67 68 69 70 71*/
			0, 0, 0, 0, 0, 0   /*72 73 74 75 76 77*/
			                   /*-----------------*/
	};


	for(int i = 0;i<78;++i){
		m.pos[i] = puyo[i];
	}

	for(int i=0; i<6*13; i++){
		m.pos_r[i] = NULL;
	}
	while(1){
		std::cout<<"リストのぷよ数を入力(1-78)"<<std::endl;
		std::cin >> list_num;
		if(list_num < 6*13 + 1 && list_num > 0) break;
		else{
			std::cout<<"1から78の数字を入力してください\n" << std::endl;
		}
	}

	if(list_num > (6*13-m.num)){
		list_num = 6 * 13 - m.num;
	}

}

Game::Game(int n){
	std::random_device rnd;     // 非決定的な乱数生成器を生成
	std::mt19937 mt(rnd());     //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
	std::uniform_int_distribution<> rand100(1, 4);        // [1, 4] 範囲の一様乱数
	int color = 0;
	m.num = n;                 //盤面のぷよ数

	for(int j = 0; j <6*13; j++) {
			m.pos[j] = EM;
			m.pos_r[j] = NULL;
		}

	for(int i = 0; i < n; ++i) {
		color = rand100(mt);
		m.pos[i] = color;
	}
	for(int i=0;i<n;++i) Game::check_ren(m.pos[i],i);
}

void Game::set_puyo(int pos, int color){
	m.pos[pos] = color;
}

void Game::print_map(int rensa_count){
	//Game::print_ren();
	std::system("clear");
	std::cout << "\r";
	for(int i = 72-6; i > -1; i -= 6){
		for(int j=0; j < 6; j++){
			Game::print(m.pos[i+j]);
			if(i+j==65) std::cout << "  ぷよ数=" << m.num;
			if(i+j==29) std::cout << "  ---" << rensa_count << "連鎖---";
		}
		std::cout << "" << std::endl;
	}
	std::this_thread::sleep_for(std::chrono::seconds(PRINT_TIME));
}

void Game::print(int color){
	switch (color)
	{
	case RE:
		std::cout << RED << "oo" << DEF;
		break;
	case GR:
		std::cout << GREEN << "oo" << DEF;
		break;
	case YE:
		std::cout << YELLOW << "oo" << DEF;
		break;
	case BL:
		std::cout << BLUE << "oo" << DEF;
		break;
	case EM:
		std::cout << EMPTY << "  " << DEF;
		break;
	}
}

int Game::do_chain(){
	ren *rr;
	int fr,fg,fy,fb;         //flag=1：消したぷよが存在する flag=0:消したぷよが存在しない
	int rensa_count = 0;    //連鎖数
	//
	//erase red puyo
	//
	while(1){
		fr=fg=fy=fb=0;
		for(int i=0;i<m.red_ren.size();++i){
			rr = m.red_ren.at(i);
			if(rr->num > 3){
				m.red_ren.erase(m.red_ren.begin() + i);
				m.num -= rr->num;
				for(int j=0; j<rr->num; ++j){
					m.pos_r[rr->pos[j]] = NULL;
					m.pos[rr->pos[j]] = EM;
				}
				if(rr) delete rr;
				fr = 1;
			}
		}
		//
		//erase green puyo
		//
		for(int i=0;i<m.green_ren.size();++i){
			rr = m.green_ren.at(i);
			if(rr->num > 3){
				m.green_ren.erase(m.green_ren.begin() + i);
				m.num -= rr->num;
				for(int j=0; j<rr->num; ++j){
					m.pos_r[rr->pos[j]] = NULL;
					m.pos[rr->pos[j]] = EM;
				}
				if(rr) delete rr;
				fg = 1;
			}
		}
		//
		//erase yellow puyo
		//
		for(int i=0;i<m.yellow_ren.size();++i){
			rr = m.yellow_ren.at(i);
			if(rr->num > 3){
				m.yellow_ren.erase(m.yellow_ren.begin() + i);
				m.num -= rr->num;
				for(int j=0; j<rr->num; ++j){
					m.pos_r[rr->pos[j]] = NULL;
					m.pos[rr->pos[j]] = EM;
				}
				if(rr) delete rr;
				fy = 1;
			}
		}
		//
		//erase blue puyo
		//
		for(int i=0;i<m.blue_ren.size();++i){
			rr = m.blue_ren.at(i);
			if(rr->num > 3){
				m.blue_ren.erase(m.blue_ren.begin() + i);
				m.num -= rr->num;
				for(int j=0; j<rr->num; ++j){
					m.pos_r[rr->pos[j]] = NULL;
					m.pos[rr->pos[j]] = EM;
				}
				if(rr) delete rr;
				fb = 1;
			}
		}
		if( !fr && !fg && !fy && !fb ) break;  //消したぷよがない場合ループを抜ける
		++rensa_count;
		Game::print_map(rensa_count);
		Game::puyo_fall();                        //落下処理
		Game::print_map(rensa_count);
	}
	return rensa_count;
}

void Game::check_ren(int color,int pos){
	int flag = 0;
	int left=pos-1,right=pos+1;
	int up=pos+6,down=pos-6;

	if(pos%6!=0 && m.pos_r[left] != NULL){  //left
		if(m.pos_r[left]->color == color){
			Game::make_ren(color, pos, left);
			flag = 1;
		}
	}
	if(pos<72 && m.pos_r[pos+6] != NULL){  //up
		if(m.pos_r[up]->color == color){
			Game::make_ren(color, pos, up);
			flag = 1;
		}
	}
	if((pos+1)%6!=0 && m.pos_r[pos+1] != NULL){  //right
		if(m.pos_r[right]->color == color){
			Game::make_ren(color, pos, right);
			flag = 1;
		}
	}
	if(pos>5 && m.pos_r[pos-6] != NULL){ //down
		if(m.pos_r[down]->color == color){
			Game::make_ren(color, pos, down);
			flag = 1;
		}
	}

	//上下左右に同色の連がない場合連を作成
	if(flag == 0){
		ren *r;
		r = new ren;
		//std::cout <<pos<<":point "<<r<<std::endl;
		r->color = color;
		r->num = 1;
		r->pos.push_back(pos);
		m.pos_r[pos] = r;

		//std::cout << r->color << " " << r->num << std::endl;
		switch(color){
		case RE:
			m.red_ren.push_back(r);
			break;
		case BL:
			m.blue_ren.push_back(r);
			break;
		case YE:
			m.yellow_ren.push_back(r);
			break;
		case GR:
			m.green_ren.push_back(r);
			break;
		}
	}
}

void Game::make_ren(int color,int pos,int dir){
	ren *r_tmp;
	//
	//posのぷよが連であるとき、探索方向の連と結合させ、posの位置にあった連情報は削除する
	//ただし探索方向の連と現在の連を指すポインタが同じ場合は除く
	//
	if(m.pos_r[pos] != NULL && m.pos_r[pos] != m.pos_r[dir]){
		m.pos_r[dir]->num += m.pos_r[pos]->num;
		r_tmp = m.pos_r[pos];
		for(int i = 0; i < r_tmp->num; ++i){
			m.pos_r[r_tmp->pos[i]] = m.pos_r[dir];   //ポインタの差し替え
			m.pos_r[dir]->pos.push_back(r_tmp->pos[i]);
		}
		switch(color){
		case RE:
			for(int j = 0; j< m.red_ren.size();++j){
				if(r_tmp == m.red_ren[j]){
					m.red_ren.erase(m.red_ren.begin() + j);
					delete r_tmp;
					break;
				}
			}
			break;
		case GR:
			for(int j = 0; j< m.green_ren.size();++j){
				if(r_tmp == m.green_ren[j]){
					m.green_ren.erase(m.green_ren.begin() + j);
					delete r_tmp;
					break;
				}
			}
			break;
		case YE:
			for(int j = 0; j< m.yellow_ren.size();++j){
				if(r_tmp == m.yellow_ren[j]){
					m.yellow_ren.erase(m.yellow_ren.begin() + j);
					delete r_tmp;
					break;
				}
			}
			break;
		case BL:
			for(int j = 0; j< m.blue_ren.size();++j){
				if(r_tmp == m.blue_ren[j]){
					m.blue_ren.erase(m.blue_ren.begin() + j);
					delete r_tmp;
					break;
				}
			}
			break;
		}
		//
		//posのぷよが連でない(初めて現れるぷよの)とき
		//
	}else if(m.pos_r[pos] == NULL){
		m.pos_r[dir]->num += 1;
		m.pos_r[dir]->pos.push_back(pos);
		m.pos_r[pos] = m.pos_r[dir];
	}
}

void Game::print_ren(){
	std::cout << "red:"<< m.red_ren.size() << std::endl;
	for(int i = 0; i < m.red_ren.size(); ++i){
		std::cout<< "num =" << m.red_ren[i]->num << std::endl;
		for(int j=0; j < m.red_ren[i]->pos.size(); ++j){
			std::cout << m.red_ren[i]->pos[j] << " ";
		}
		std::cout << "" <<std::endl;
	}

	std::cout << "\nblue:"<< m.blue_ren.size() << std::endl;
	for(int i = 0; i < m.blue_ren.size(); ++i){
		std::cout<< "num =" << m.blue_ren[i]->num << std::endl;
		for(int j=0; j < m.blue_ren[i]->pos.size(); ++j){
			std::cout << m.blue_ren[i]->pos[j] << " ";
		}
		std::cout << "" <<std::endl;
	}

	std::cout << "\nyellow:"<< m.yellow_ren.size() << std::endl;
	for(int i = 0; i < m.yellow_ren.size(); ++i){
		std::cout<< "num =" << m.yellow_ren[i]->num << std::endl;
		for(int j=0; j < m.yellow_ren[i]->pos.size(); ++j){
			std::cout << m.yellow_ren[i]->pos[j] << " ";
		}
		std::cout << "" <<std::endl;
	}

	std::cout << "\ngreen:"<< m.green_ren.size() << std::endl;
	for(int i = 0; i < m.green_ren.size(); ++i){
		std::cout<< "num =" << m.green_ren[i]->num << std::endl;
		for(int j=0; j < m.green_ren[i]->pos.size(); ++j){
			std::cout << m.green_ren[i]->pos[j] << " ";
		}
		std::cout << "" <<std::endl;
	}
}


void Game::puyo_fall(){
	int bottom,color;
	ren* r;
	for(int i=0;i < 6; ++i){
		bottom = 99;
		for(int j = 0; j < i+73; j+=6){
			if(m.pos[i+j]==EM && (i+j) < bottom){
				bottom = i+j;          //何もないぷよの位置をbottomに記録
			}
			else if((bottom < (6*13+1) && bottom > -1) && m.pos[i+j] != EM){  //空白箇所が確認されていて、その上にぷよがあるとき
				//ぷよの色について更新
				color = m.pos[bottom] = m.pos[i+j];
				m.pos[i+j] = EM;
				//ぷよのポインタを更新
				r = new ren;
				r->color = color;
				r->num = 1;
				r->pos.push_back(bottom);
				m.pos_r[bottom] = r;
				switch(color){
				case RE:
					for(int k = 0; m.red_ren.size(); ++k){
						if(m.red_ren[k] == m.pos_r[i+j]){
							m.red_ren[k]->num -= 1;
							for(int s = 0; m.red_ren[k]->pos.size(); ++s){
								if(m.red_ren[k]->pos[s] == (i+j)) {
									m.red_ren[k]->pos.erase(m.red_ren[k]->pos.begin() + s);
									break;
								}
							}
							break;
						}
					}
					break;
				case GR:
					for(int k = 0; m.green_ren.size(); ++k){
						if(m.green_ren[k] == m.pos_r[i+j]){
							m.green_ren[k]->num -= 1;
							for(int s = 0; m.green_ren[k]->pos.size(); ++s){
								if(m.green_ren[k]->pos[s] == (i+j)) {
									m.green_ren[k]->pos.erase(m.green_ren[k]->pos.begin() + s);
									break;
								}
							}
							break;
						}
					}
					break;
				case YE:
					for(int k = 0; m.yellow_ren.size(); ++k){
						if(m.yellow_ren[k] == m.pos_r[i+j]){
							m.yellow_ren[k]->num -= 1;
							for(int s = 0; m.yellow_ren[k]->pos.size(); ++s){
								if(m.yellow_ren[k]->pos[s] == (i+j)) {
									m.yellow_ren[k]->pos.erase(m.yellow_ren[k]->pos.begin() + s);
									break;
								}
							}
							break;
						}
					}
					break;
				case BL:
					for(int k = 0; m.blue_ren.size(); ++k){
						if(m.blue_ren[k] == m.pos_r[i+j]){
							m.blue_ren[k]->num -= 1;
							for(int s = 0; m.blue_ren[k]->pos.size(); ++s){
								if(m.blue_ren[k]->pos[s] == (i+j)) {
									m.blue_ren[k]->pos.erase(m.blue_ren[k]->pos.begin() + s);
									break;
								}
							}
							break;
						}
					}
					break;
				}
				m.pos_r[i+j] = NULL;
				//連を作り直す
				Game::check_ren(color, bottom);
				//もう一度探索する
				j = 0;
				bottom = 99;
			}
		}
	}
}
