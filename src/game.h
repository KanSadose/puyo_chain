/*
 * game.h
 *
 *  Created on: 2017/06/15
 *      Author: sadose
 */

#ifndef GAME_H_
#define GAME_H_

#include <chrono>
#include <thread>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <random>
#include <vector>

#define DEF "\x1b[49m"
#define RED "\x1b[41m"
#define GREEN "\x1b[42m"
#define YELLOW "\x1b[43m"
#define BLUE "\x1b[44m"
#define EMPTY "\x1b[47m"

#define RE 1 //赤
#define GR 2 //緑
#define YE 3 //黄
#define BL 4 //青
#define EM 0 //何もなし

#define LEFT 1
#define UP 2
#define RIGHT 3
#define DOWN 4

#define PRINT_TIME 2 //描画時間(s)

/*-----------------*/
/*72 73 74 75 76 77*/
/*66 67 68 69 70 71*/
/*60 61 62 63 64 65*/
/*54 55 56 57 58 59*/
/*48 49 50 51 52 53*/
/*42 43 44 45 46 47*/
/*36 37 38 39 40 41*/
/*30 31 32 33 34 35*/
/*24 25 26 27 28 29*/
/*18 19 20 21 22 23*/
/*12 13 14 15 16 17*/
/*6  7  8  9  10 11*/
/*0  1  2  3  4  5 */
/*-----------------*/


struct ren{
	int num;    //連に属するぷよの数
	int color;  //ぷよの色
	std::vector<int> pos; //連に属するぷよの位置
};

struct map{
	int pos[6*13]; //盤面のぷよの色を記録
	int num;    //盤面のぷよ数
	int list[6*13];
	ren *pos_r[6*13];
	std::vector<ren*> red_ren; //赤ぷよの連を格納するコンテナ
	std::vector<ren*> blue_ren;
	std::vector<ren*> yellow_ren;
	std::vector<ren*> green_ren;
};

class Game{
private:
	map m;
public:
	Game();                            //盤面を端末から入力
	Game(int n); 					   //盤面を指定したぷよ数で初期化
	void print_map(int rensa_count);	//盤面の表示
	void print(int color);             //ぷよの色を引数に取り描画する
	void set_puyo(int pos,int color);  //位置、色を引数にとりmap構造体に代入する
	int do_chain();                   //盤面のぷよから連鎖数を計算する(実際に連鎖、落下処理も行う)
	void check_ren(int color,int pos); //ぷよの色と位置を引数に取り連を生成できるか調べる
	void make_ren(int color,int pos,int dir);  //ぷよの色と位置、探索方向を引数に取り連を作成
	void print_ren();                  //連の確認用関数
	void puyo_fall();                  //ぷよを消したあとぷよを落下させる
};


#endif /* GAME_H_ */
